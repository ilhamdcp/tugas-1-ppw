from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_status, delete_status
from .models import Status
from .forms import Status_Form

# Create your tests here.

class UpdateStatusUnitTest(TestCase):
    def test_update_status_url_is_exist(self):
        response = Client().get('/update-status/')
        self.assertEqual(response.status_code, 200)

    def test_update_status_using_index_func(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        #Creating a new status
        new_status = Status.objects.create(message='This is a test')

        #Retrieving all available activity
        counting_all_available_status= Status.objects.all().count()
        self.assertEqual(counting_all_available_status,1)

    def test_form_status_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="form-control"', form.as_p())
        
    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'name': '', 'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )
    
    def test_update_status_post_fail(self):
        response = Client().post('/update-status/add_status', {'name': 'Team 7', 'message': ''})
        self.assertEqual(response.status_code, 302)

    def test_update_status_success_and_render_the_result(self):
        name = 'Team 7'
        message = 'HaiHai'
        response_post = Client().post('/update-status/add_status', {'name': name, 'message': message})
        self.assertEqual(response_post.status_code, 302)
        
        response = Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(message,html_response)

    def test_isi_message_sesuai(self):
        statusnya = Status(message="apakah pesan ini sama?")
        self.assertEqual(str(statusnya), "apakah pesan ini sama?")

    def test_update_status_delete(self):
        pesan='status ini akan didelete'
        request = HttpRequest()
        new_status = Status.objects.create(message=pesan)

        # periksa apakah status sudah terbuat
        response = Client().get('/update-status/')
        html_response = response.content.decode('utf-8')
        self.assertIn(pesan, html_response)

        # delete dan update html_response
        delete_status(request, new_status.id)
        response = Client().get('/update-status/')
        html_response = response.content.decode('utf-8')
        self.assertNotIn(pesan, html_response)

# Create your tests here.
