from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from halaman_profile.models import Profile

# Create your views here.
response = {'author':'Kelompok 7 PPW-F', 'name' : Profile.name, 'status_form': Status_Form}

def index(request):
    html = 'update_status.html'
    allStatus = Status.objects.all()
    response['allStatus'] = allStatus
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        status = Status(name=response['name'], message=response['message'])
        status.save()
        allStatus = Status.objects.all()
        response['allStatus'] = allStatus
        html = 'update_status.html'
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')

def delete_status(request,id):
    status_target = Status.objects.all().get(pk=id)
    status_target.delete()
    return HttpResponseRedirect('/update-status/')