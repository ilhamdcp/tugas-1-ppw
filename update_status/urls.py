from django.conf.urls import url
from .views import delete_status, add_status, index

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^delete_status/(?P<id>[0-9999]+)/$', delete_status, name='delete_status'),
]
