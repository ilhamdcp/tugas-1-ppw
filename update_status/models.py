from django.db import models

class Status(models.Model):
    name = models.CharField(max_length=27)
    message = models.TextField(max_length=300)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message

