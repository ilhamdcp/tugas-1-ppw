from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index


# Create your tests here.

class ProfileUnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response_views = Client().get('/profile/')
        self.assertEqual(response_views.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_name_is_changed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("</table>", html_response)
