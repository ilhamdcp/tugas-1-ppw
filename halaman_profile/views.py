from django.shortcuts import render
from .models import Profile
response = {}
keahlian = ['ngoding', 'adbis', 'main game', 'PPW SERU']


def index(request):
    response['author'] = 'Kelompok 7 PPW-F'
    response['name'] = Profile.name
    response['birthday'] = Profile.birthday
    response['email'] = Profile.email
    response['gender'] = Profile.gender
    response['expertise'] = keahlian
    response['description'] = Profile.description
    return render(request, 'profile.html', response)

# Create your views here.
