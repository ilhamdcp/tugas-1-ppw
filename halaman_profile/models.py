# Create your models here.
from django.db import models


class Profile(models.Model):
    name = 'Team 7'
    birthday = 'Nov 22'
    gender = 'Male'
    description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    email = 'dpraharsa@yahoo.com'
