from django.db import models


class Friend(models.Model):
    name = models.CharField(max_length=27)
    url = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'name: {}, url: {} added on {}'.format(self.name, self.url, self.created_date)
