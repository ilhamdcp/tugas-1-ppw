from django.test import TestCase, Client
from django.urls import resolve
from .models import Friend
from .views import index, response
from .forms import Friend_Form


# Create your tests here.
class AddFriendUnitTest(TestCase):
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index)

    def test_models_create_new_object(self):
        # new activity
        create_object = Friend.objects.create(name='Test', url='https://this_is_a_test.com')

        # counting objects in models
        count = Friend.objects.all().count()
        self.assertEqual(count, 1)

    def test_response_size_increasing_and_having_some_elements(self):
        found = resolve('/add-friend/')
        self.assertTrue(len(response) > 0)
        self.assertIn('author', response)
        self.assertIn('friend_form', response)
        self.assertIn('friends', response)

    def test_form_friend_input_has_placeholder_and_css_classes(self):
        form = Friend_Form()
        self.assertIn('class="todo-form-input', form.as_p())
        self.assertIn('id="id_name"', form.as_p())
        self.assertIn('class="todo-form-textarea', form.as_p())
        self.assertIn('id="id_url', form.as_p())

    def test_models_str(self):
        name1 = 'Tester'
        url1 = 'https;//this-is-a-test'
        Friend.objects.create(name=name1, url=url1)
        an_object = Friend.objects.get(name='Tester')
        self.assertEqual(an_object.__str__(),
                         'name: {}, url: {} added on {}'.format(name1, url1, an_object.created_date))

    def test_add_friend(self):
        name = 'test'
        url = 'https://this-is-a-test.herokuapp.com/'
        post = Client().post('/add-friend/add', {'name': name, 'url': url})
        self.assertEqual(post.status_code, 302)

        check = Client().get('/add-friend/')
        html_check = check.content.decode('utf8')
        self.assertIn(name, html_check)
        self.assertIn(url, html_check)

    def test_add_friend_invalid(self):
        name = url = 'test'
        post = Client().post('/add-friend/add', {'name': name, 'url': url})
        self.assertEqual(post.status_code, 200)

        check = Client().get('/add-friend/')
        html_check = check.content.decode('utf8')
        self.assertNotIn(name, html_check)
        self.assertNotIn(url, html_check)
