from django import forms
from django.core.validators import RegexValidator


class Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi url yang valid',
    }
    name_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder': 'Nama...'
    }
    url_attrs = {
        'type': 'text',
        'class': 'todo-form-textarea',
        'placeholder': 'contoh: https://x.herokuapp.com/'
    }

    name = forms.CharField(label='Nama ', required=True, max_length=40,
                           widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label='url ', required=True, validators=[RegexValidator(r'herokuapp.com/$')], error_messages=error_messages,
                         widget=forms.URLInput(attrs=url_attrs))
