from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

response = {}


def index(request):
    response['author'] = 'Kelompok 7 PPW-F'
    response['friend_form'] = Friend_Form
    response['friends'] = Friend.objects.all()
    return render(request, 'add_friend_page.html', response)


def add_friend(request):
    form = Friend_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friends = Friend(name=response['name'], url=response['url'])
        friends.save()
        response['error'] = False
        return HttpResponseRedirect('/add-friend/')
    else:
        response['error'] = True
        return render (request,'add_friend_page.html',response)
