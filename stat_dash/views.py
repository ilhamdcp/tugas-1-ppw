from django.shortcuts import render
from add_friend.models import Friend
from update_status.models import Status
from halaman_profile.models import Profile

# Create your views here.

response = {}


def index(request):
    response['latest'] = ''
    response['date'] = ''
    if Status.objects.all().count() > 0:
        latestStatus = Status.objects.all().order_by('-created_date')[0]
        response['latest'] = latestStatus.message
        response['date'] = latestStatus.created_date
    response['author'] = 'Kelompok 7 PPW-F'
    response['name'] = Profile.name
    response['friend'] = Friend.objects.all().count()
    response['feed'] = Status.objects.all().count()
    return render(request, 'dashboard.html', response)
