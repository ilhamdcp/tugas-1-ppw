from django.apps import AppConfig


class StatDashConfig(AppConfig):
    name = 'stat_dash'
