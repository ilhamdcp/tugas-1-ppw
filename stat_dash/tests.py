from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, response
from add_friend.models import Friend
from update_status.models import Status


# Create your tests here.
class StatDashUnitTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response_views = Client().get('/dashboard/')
        self.assertEqual(response_views.status_code, 200)

    def test_dashboard_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)

    def test_response_dictionary_update(self):
        # new activity
        Friend.objects.create(name='Test', url='https://this_is_a_test.com')
        Status.objects.create(name='Test', message='This is a test')
        requesting = Client().get('/dashboard/')

        # counting objects in models
        count_friend = response['friend']
        count_feed = response['feed']

        # checking that the response dictionary is updated
        self.assertEqual(count_friend, 1)
        self.assertEqual(count_feed, 1)

    def test_dashboard_card(self):
        list_of_response_dict = {'author': 'Team 7', 'name': 'Team 7', 'friend': '0', 'feed': '0'}
        request = HttpRequest()
        rsp = index(request)
        html_response = rsp.content.decode('utf8')
        self.assertIn('</h4>', html_response)
        for i in list_of_response_dict:
            self.assertIn(list_of_response_dict[i], html_response)
