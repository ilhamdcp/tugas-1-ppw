from django.shortcuts import render
from halaman_profile.models import Profile

# Create your views here.
def index(request):
    response={'author':'Kelompok 7 PPW-F', 'name' : Profile.name}
    html = 'LandingPage.html'
    return render(request, html, response)